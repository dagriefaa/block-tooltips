[img=https://i.imgur.com/Qz5d98H.png][/img]
                             [b][url=https://besiege.fandom.com]Besiege Wiki[/url][/b]  |  [b][url=https://gitlab.com/dagriefaa/block-tooltips]GitLab Repository[/url][/b]

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2913469777][img]https://i.imgur.com/XGCiJRy.png[/img][/url]

[img=https://i.imgur.com/9rOFiPs.png][/img]

Adds more informative and more standardised tooltips to blocks in the block toolbar.

To some extent, it's expected that you know how blocks in Besiege work and what these values mean.
[url=https://besiege.fandom.com/wiki/Besiege_Wiki]If you don't, there's always the wiki.[/url]

This mod took less than a day to make using UIFactory. Without it, either it would have looked nastier or taken a whole week.