<div align="center">
<img src="header.png" alt="Better Block Tooltips" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=2913470285<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/2913470285?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/2913470285?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/2913470285?color=%231b2838&logo=steam&style=for-the-badge)
</div>

This mod replaces the default block toolbar tooltips with something more useful to seasoned players.

<div align="center"><img src="https://i.imgur.com/9rOFiPs.png" width="500" /></div>

It is expected that you understand what these values are without needing me to explain them.

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
You will need to install [UIFactory 3](https://gitlab.com/dagriefaa/ui-factory-3) to use this mod. Follow the instructions to install that first. <br>
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Install [UIFactory 3](https://gitlab.com/dagriefaa/ui-factory-3).
3. Clone the repository into `Besiege/Besiege_Data/Mods`.
4. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
5. Set up references to `Besiege.UI.dll` and `Besiege.UI.Bridge.dll` from UIFactory 3.
6. Press F6 to compile the mod.

## Usage
If the usage of this mod isn't self-evident, I don't know what to tell you.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
