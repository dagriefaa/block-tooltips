﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlockTooltips {
    internal class TooltipListener : ClickBehaviour {

        public Besiege.UI.Bridge.Tooltip self;

        public override void OnClicked() {
            base.OnClicked();
            self.OnPointerClick(null);
        }

        void OnMouseOver() {
            self.OnPointerEnter(null);
        }

        void OnMouseExit() {
            self.OnPointerExit(null);
        }
    }
}
