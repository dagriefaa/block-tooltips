﻿using Besiege.UI;
using Besiege.UI.Extensions;
using Besiege.UI.Serialization;
using Modding;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BlockTooltips {
    public class Mod : ModEntryPoint {
        public const string MOD_NAME = "BlockTooltips";
        public static GameObject ModControllerObject;

        public static readonly string[] TAB_NAMES = {
            "t_BASIC",
            "t_LOCOMOTION",
            "t_MECHANICAL",
            "t_WEAPONRY",
            "t_FLIGHT",
            "t_ARMOUR",
            "t_LOGIC",
            "t_WATER"
        };

        public static readonly HashSet<BlockType> IGNORED_BLOCKS = new HashSet<BlockType> {
            BlockType.StartingBlock,
            BlockType.Magnet,
            BlockType.Unused,
        };

        public static readonly HashSet<BlockType> INTANGIBLE_BLOCKS = new HashSet<BlockType> {
            BlockType.Pin,
            BlockType.CameraBlock
        };

        public static readonly HashSet<BlockType> FLIP_BLOCKS = new HashSet<BlockType> {
            BlockType.Wheel,
            BlockType.LargeWheel,
            BlockType.CogMediumPowered,
            BlockType.SteeringHinge,
            BlockType.SteeringBlock,
            BlockType.SpinningBlock,
            BlockType.Piston,
            BlockType.Slider,
            BlockType.CircularSaw,
            BlockType.Drill,
            BlockType.Propeller,
            BlockType.SmallPropeller,
            BlockType.Rudder,
            BlockType.NauticalScrew
        };

        public static Dictionary<BlockType, string> BLOCK_MASS = new Dictionary<BlockType, string> {
            { BlockType.Ballast, "0.2 ~ 3.0" },
            { BlockType.ScalingBlock, "0.2 ~ 2.0" },
            { BlockType.BuildSurface, "0.1 ~ ∞" }
        };

        public static Dictionary<BlockType, string> BLOCK_DESCRIPTIONS = new Dictionary<BlockType, string> {
            { BlockType.Brace, "<color=grey>CLICK AND DRAG TO PLACE" +
                "\n" +
                "\nSECOND ENDPOINT CAN CONNECT MULTIPLE BLOCKS</color>" },
            { BlockType.ScalingBlock, "<color=grey>ADJUSTABLE MASS</color>" },

            { BlockType.Axle, "<color=grey>TRANSMITS ROTATION" +
                "\n AT AN ANGLE</color>" },
            { BlockType.SteeringHinge, "<color=grey>CONTROL WITH</color> [◁] [▷]\n" +
                "<color=grey>1x = 100°/SEC</color>" },
            { BlockType.SteeringBlock, "<color=grey>CONTROL WITH</color> [◁] [▷]\n" +
                "<color=grey>1x = 80°/SEC</color>" },
            { BlockType.Wheel, "<color=grey>CONTROL WITH</color> [△] [▽]" },
            { BlockType.LargeWheel, "<color=grey>CONTROL WITH</color> [△] [▽]" },
            { BlockType.CogMediumPowered, "<color=grey>CONTROL WITH</color> [△] [▽]" },
            { BlockType.SpinningBlock, "<color=grey>AUTO-ROTATES</color>" },
            { BlockType.Piston, "<color=grey>EXTEND WITH</color> [H]" },
            { BlockType.Decoupler, "<color=grey>DISCONNECT WITH</color> [J]" },
            { BlockType.Grabber, "<color=grey>GRAB WITH</color> [V]" },
            { BlockType.Spring, "<color=grey>CLICK AND DRAG TO PLACE</color>\n" +
                "<color=grey>CONTRACT WITH</color> [V]" },
            { BlockType.RopeWinch, "<color=grey>CLICK AND DRAG TO PLACE</color>\n" +
                "<color=grey>WIND WITH</color> [N]\n" +
                "<color=grey>UNWIND WITH</color> [M]" },

            { BlockType.CircularSaw, "<color=grey>AUTO-ROTATES</color>" },
            { BlockType.Drill, "<color=grey>AUTO-ROTATES</color>" },
            { BlockType.MetalJaw, "<color=grey>RELEASE WITH</color> [C]\n" +
                "<color=grey>MUST WIND UP FIRST</color>\n" +
                "\n" +
                "<color=grey>DOESN'T PHASE AT\n" +
                "HIGH SPEEDS</color>" },
            { BlockType.Cannon, "<color=grey>FIRE WITH</color> [C]\n" +
                "<color=grey>ONLY 1 SHOT</color>\n" +
                "\n" +
                "<color=red>SHOOTS IF HEATED</color>" },
            { BlockType.Crossbow, "<color=grey>FIRE WITH</color> [C]\n" +
                "<color=grey>HAS 30 SHOTS</color>" },
            { BlockType.Flamethrower, "<color=grey>BLAST WITH</color> [Y]\n" +
                "<color=grey>LASTS 10 SECONDS</color>" },
            { BlockType.Vacuum, "<color=grey>SUCC WITH</color> [Y]" },
            { BlockType.ShrapnelCannon, "<color=grey>FIRE WITH</color> [C]\n" +
                "<color=grey>ONLY 1 SHOT</color>\n" +
                "\n" +
                "<color=red>SHOOTS IF HEATED</color>" },
            { BlockType.WaterCannon, "<color=grey>SPRAY WITH</color> [Y]\n" +
                "<color=grey>EXTINGUISHES FIRE\n" +
                "\n" +
                "RECOIL FORCE:\n" +
                "IF POWER < 1\n→ POWER x 10 \n" +
                "ELSE\n→ POWER x 14 - 4\n" +
                "\n" +
                "HEAT TO INCREASE POWER 8x</color>" },
            { BlockType.Torch, "<color=red>STARTS ON FIRE</color>" },
            { BlockType.Bomb, "<color=red>EXPLODES ON IMPACT</color>" },
            { BlockType.Grenade, "<color=grey>EXPLODE WITH</color> [Y]" },
            { BlockType.Rocket, "<color=grey>FIRE WITH</color> [Y]\n" +
                "<color=grey>DOES NOT EXPLODE ON IMPACT</color>" },
            { BlockType.FlameBall, "<color=red>STARTS ON FIRE</color>" },
            { BlockType.Boulder, "<color=grey>BREAKS ON IMPACT</color>" },

            { BlockType.BuildSurface, "<color=grey>CLICK TO PLACE CORNERS\n" +
                "4 CORNERS: SQUARE\n" +
                "3 CORNERS + FIRST: TRIANGLE\n" +
                "\n" +
                "ADJUST WITH ADVANCED BUILDING\n" +
                "\n" +
                "VARIABLE MASS AND HEALTH\n" +
                "HAS OPTIONAL DRAG</color>" },
            { BlockType.GripPad, "<color=grey>HIGH FRICTION</color>" },
            { BlockType.Plow, "<color=grey>LOW FRICTION</color>" },
            { BlockType.HalfPipe, "<color=grey>LOW FRICTION</color>" },
            { BlockType.BombHolder, "<color=grey>DOESN'T ACTUALLY GRAB THINGS</color>" },

            { BlockType.FlyingBlock, "<color=grey>PROPEL WITH</color> [O]\n" +
                "\n" +
                "<color=grey>RECOIL FORCE:\n" +
                "POWER x 100</color>" },
            { BlockType.Propeller, "<color=grey>HAS LIFT\n" +
                "FLATANGLE: 23.06876°</color>" },
            { BlockType.SmallPropeller, "<color=grey>HAS LIFT\n" +
                "FLATANGLE: 22.845°</color>" },
            { BlockType.Wing, "<color=grey>HAS DRAG</color>" },
            { BlockType.WingPanel, "<color=grey>HAS DRAG</color>" },
            { BlockType.Ballast, "<color=grey>VARIABLE MASS</color>" },
            { BlockType.Balloon, "<color=grey>HAS BUOYANCY</color>" },
            { BlockType.SqrBalloon, "<color=grey>RISE WITH </color> [U]\n" +
                "<color=grey>FALL WITH </color> [J]\n" +
                "\n" +
                "<color=red>IGNITES ON BREAK</color>" },

            { BlockType.Sensor, "<color=grey>DETECTS OBJECTS\n" +
                "\n" +
                "EMULATES </color> [C]" },
            { BlockType.Timer, "<color=grey>START WITH</color> [B]\n" +
                "\n" +
                "<color=grey>EMULATES</color> [C] <color=grey>AFTER TIME</color>" },
            { BlockType.Altimeter, "<color=grey>DETECTS ALTITUDE\n" +
                "\n" +
                "EMULATES </color> [C]" },
            { BlockType.LogicGate, "<color=grey>TAKES TWO KEYS\n" +
                "\n" +
                "EMULATES </color> [C] <color=grey>WHEN CONDITION MET</color>" },
            { BlockType.Anglometer, "<color=grey>TRACKS ANGLE\n" +
                "\n" +
                "EMULATES </color> [C] <color=grey>INSIDE RANGE</color>" },
            { BlockType.Speedometer, "<color=grey>DETECTS SPEED\n" +
                "\n" +
                "EMULATES </color> [C]" },
            { BlockType.RopeMeasure, "<color=grey>DETECTS DISTANCE\n" +
                "\n" +
                "EMULATES </color> [C]" },
            { BlockType.Pin, "<color=grey>FREEZES BLOCKS IN PLACE\n" +
                "\n" +
                "UNPIN WITH</color> [P]" },
            { BlockType.CameraBlock, "<color=grey>SHOW FIXED VIEWPOINT\n" +
                "\n" +
                "SHOW WITH</color> [F]" },
            { BlockType.Buoyancy, "<color=grey>BUOYANT IN WATER\n" +
                "RISE WITH </color> [U]\n" +
                "<color=grey>SINK WITH </color> [J]"},
            { BlockType.BigBarrel, "<color=grey>BUOYANT IN WATER\n" +
                "RISE WITH </color> [U]\n" +
                "<color=grey>SINK WITH </color> [J]"},
            { BlockType.Rudder, "<color=grey>CONTROL WITH</color> [◁] [▷]" },
            { BlockType.NauticalScrew, "<color=grey>PROPELS IN WATER\n" +
                "CONTROL WITH</color> [△] [▽]" },
            { BlockType.Paddle, "<color=grey>HAS DRAG IN WATER</color>" },
            { BlockType.Sail, "<color=grey>PROPELS WHEN UNFURLED\n" +
                "UNFURL WITH </color> [K]\n" +
                "<color=grey>FURL WITH </color> [I]"},
            { BlockType.Harpoon, "<color=grey>FIRE WITH</color> [C]\n" +
                "<color=grey>AND HOLD TO RETRACT\n\n" +
                "RELEASE AND RELOAD WITH</color> [V]" },
            { BlockType.BouncyPad, "<color=grey>VERY BOUNCY</color>\n" +
                "\n" +
                "<color=grey>DOESN'T PHASE AT\n" +
                "HIGH SPEEDS</color>" },
            { BlockType.FlyWheel, "<color=grey>HIGH INERTIA\n" +
                "\n" +
                "LOCK/UNLOCK WITH</color> [J]" },
            { BlockType.DragBlock, "<color=grey>HAS\n" +
                "DIRECTIONAL DRAG</color>" },
        };

        public override void OnLoad() {
            // Init common mod controller object
            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) { UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject")); }

            // Load Object Explorer mappings
            if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                //ModControllerObject.AddComponent<Mapper>();
            }

            // register serialization provider
            Make.RegisterSerialisationProvider(MOD_NAME, new SerializationProvider {
                CreateText = p => Modding.ModIO.CreateText(p, false),
                ReadAllText = p => Modding.ModIO.ReadAllText(p, false),
                GetFiles = p => Modding.ModIO.GetFiles(p, false),
                AllResourcesLoaded = () => ModResource.AllResourcesLoaded,
                OnAllResourcesLoaded = x => ModResource.OnAllResourcesLoaded += x,
            });

            Make.RegisterSprite(Mod.MOD_NAME, "IceIcon", ModResource.GetTexture("IceIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "FrictionIcon", ModResource.GetTexture("FrictionIcon"));

            // OnSceneChanged
            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }
        }

        static void OnSceneChanged(Scene a, Scene b) {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            Make.OnReady(Mod.MOD_NAME, () => {
                var stopwatch = new System.Diagnostics.Stopwatch();
                stopwatch.Start();

                GameObject tooltipPrefab = null;
                foreach (string tab in TAB_NAMES) {
                    Transform tabObject = HierarchyUtils.FindObject("HUD/BottomBar/Align (Bottom Left)/BLOCK BUTTONS/" + tab);
                    if (!tabObject) {
                        continue;
                    }
                    foreach (BlockButtonControl blockButton in tabObject.GetComponentsInChildren<BlockButtonControl>(true)) {
                        if (IGNORED_BLOCKS.Contains((BlockType)blockButton.myIndex)) {
                            continue;
                        }
                        if (!PrefabMaster.GetBlock((BlockType)blockButton.myIndex, out BlockBehaviour block)) {
                            continue;
                        }

                        GameObject tooltip;
                        if (!tooltipPrefab) {
                            tooltip = tooltipPrefab = Make.LoadProject(MOD_NAME, "Tooltip", blockButton.transform).gameObject;
                        }
                        else {
                            tooltip = GameObject.Instantiate(tooltipPrefab, blockButton.transform, false) as GameObject;
                        }
                        tooltip.name = "Tooltip";
                        blockButton.gameObject.AddComponent<TooltipListener>().self = tooltip.GetComponentInChildren<Besiege.UI.Bridge.Tooltip>();
                        blockButton.GetComponent<Tooltip>().enabled = false;

                        BLOCK_MASS.TryGetValue(block.Prefab.Type, out string mass);
                        BLOCK_DESCRIPTIONS.TryGetValue(block.Prefab.Type, out string description);

                        Project project = tooltip.GetComponent<Project>();
                        project.RebuildTransformList();

                        // name and id
                        project["BlockName"].GetComponent<Text>().text = ReferenceMaster.TranslateBlockName(block.Prefab.Type);
                        project["BlockID"].GetComponent<Text>().text = $"{blockButton.myIndex}: {block.Prefab.name}";

                        // break types
                        project["BreakFire"].GetComponent<Image>().color = block.fireTag ? Color.white : new Color(1, 1, 1, 0.5f);
                        project["BreakIce"].GetComponent<Image>().color = block.iceTag ? Color.white : new Color(1, 1, 1, 0.5f);
                        project["BreakDamageIcon"].GetComponent<Image>().color = block.BlockHealth ? Color.white : new Color(1, 1, 1, 0.5f);
                        project["BreakDamage"].GetComponent<Text>().text = $"{block.BlockHealth?.health ?? 0}";
                        project["BreakDamage"].gameObject.SetActive(block.BlockHealth != null && block.BlockHealth.health > 0);

                        project["BlockBreakStates"].gameObject.SetActive(!INTANGIBLE_BLOCKS.Contains(block.Prefab.Type));

                        // mass
                        if (mass != null) {
                            Text massText = project["BlockMass"].GetComponent<Text>();
                            massText.text = mass;
                            massText.fontSize = 14;
                            project["BlockMass"].transform.parent.gameObject.SetActive(true);
                        }
                        else {
                            project["BlockMass"].GetComponent<Text>().text = $"{block.Rigidbody.mass:0.00}";
                            project["BlockMass"].transform.parent.gameObject.SetActive(!INTANGIBLE_BLOCKS.Contains(block.Prefab.Type) && block.Rigidbody.mass > 0);
                        }

                        // drag
                        project["BlockDrag"].GetComponent<Text>().text = $"{block.Rigidbody.drag:0.00}";
                        project["BlockDrag"].transform.parent.gameObject.SetActive(block.Rigidbody.drag > 0);

                        DoFriction(block, project);

                        // joint strengths
                        DoPrimaryJoint(block, project);
                        DoSecondaryJoint(block, project);

                        // miscellaneous bs
                        project["BlockDescription"].GetComponent<Text>().text = description;
                        project["BlockDescription"].gameObject.SetActive(description != null);

                        // flippable
                        project["BlockFlip"].gameObject.SetActive(FLIP_BLOCKS.Contains(block.Prefab.Type));
                    }

                }
                stopwatch.Stop();
                Debug.Log($"[BlockTooltips] Loaded tooltips in {stopwatch.ElapsedMilliseconds}ms");
            });
        }

        private static void DoFriction(BlockBehaviour block, Project project) {
            if (INTANGIBLE_BLOCKS.Contains(block.Prefab.Type)) {
                project["BlockFriction"].transform.parent.gameObject.SetActive(false);
                return;
            }
            project["BlockFriction"].transform.parent.gameObject.SetActive(true);
            if (block.Prefab.Type == BlockType.BuildSurface) {
                project["BlockFriction"].GetComponent<Text>().text = "0.6";
                return;
            }

            Collider solid = block.GetComponentsInChildren<Collider>().FirstOrDefault(
                c => ((c.gameObject.layer is 12 or 14) && !(c.isTrigger || block is SqrBalloonController))
                    || ((c.gameObject.layer is 0 or 25 or 26 or 15)
                        && !(block is SliderCompress && c.gameObject == block.gameObject)
                        && !(block is BuildSurface)
                        && !(block is GrabberBlock && c.GetComponent<JoinOnTriggerBlock>())));
            if (solid) {
                project["BlockFriction"].GetComponent<Text>().text = $"{solid.material.dynamicFriction:0.00}";
                return;
            }

            project["BlockFriction"].transform.parent.gameObject.SetActive(false);
        }

        private static void DoPrimaryJoint(BlockBehaviour block, Project project) {
            if (INTANGIBLE_BLOCKS.Contains(block.Prefab.Type)) {
                project["BlockBreakForce1"].transform.parent.gameObject.SetActive(false);
                return;
            }
            project["BlockBreakForce1"].transform.parent.gameObject.SetActive(true);
            if (block.Prefab.Type == BlockType.BuildSurface) {
                project["BlockBreakForce1"].GetComponent<Text>().text = "500 ~ 8M";
                return;
            }

            if (block.CompareTag("MechanicalTag")) {
                project["BlockBreakForce1Icon"].GetComponent<Image>().color = new Color(1, 0.65f, 0.65f);
            }

            if (block.Prefab.Type == BlockType.SpinningBlock || block.Prefab.Type == BlockType.CircularSaw) {
                project["BlockBreakForce1"].GetComponent<Text>().text = "∞";
                return;
            }
            HingeJoint mechJoint = block.GetComponent<HingeJoint>();
            if (mechJoint) {
                project["BlockBreakForce1"].GetComponent<Text>().text = $"{mechJoint.breakForce}";
                return;
            }
            ConfigurableJoint joint = block.GetComponentInChildren<ConfigurableJoint>();
            if (joint) {
                string text = float.IsInfinity(joint.breakForce) ? "∞" : $"{joint.breakForce}";
                if (block.GetComponent<ReduceBreakForceOnImpact>()) { // tick damage
                    text += "*";
                }
                project["BlockBreakForce1"].GetComponent<Text>().text = text;
                return;
            }
            project["BlockBreakForce1"].transform.parent.gameObject.SetActive(false);
        }

        private static void DoSecondaryJoint(BlockBehaviour block, Project project) {
            if (INTANGIBLE_BLOCKS.Contains(block.Prefab.Type)) {
                project["BlockBreakForce2"].transform.parent.gameObject.SetActive(false);
                return;
            }
            project["BlockBreakForce2"].transform.parent.gameObject.SetActive(true);
            var secondJoint = block.GetComponentInChildren<TriggerSetJoint2>();
            if (secondJoint) {
                project["BlockBreakForce2"].GetComponent<Text>().text = $"{secondJoint.jointToCopy.breakForce}";
                return;
            }
            if (block.Prefab.Type == BlockType.Brace) {
                project["BlockBreakForce2Icon"].GetComponent<Image>().color = Color.yellow;
                project["BlockBreakForce2"].GetComponent<Text>().text = "7700";
                return;
            }
            project["BlockBreakForce2"].transform.parent.gameObject.SetActive(false);
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

        /// <see href="https://stackoverflow.com/a/34006336" />
        public static int CustomHash(params int[] vals) {
            int hash = 1009;
            foreach (int i in vals) {
                hash = (hash * 9176) + i;
            }
            return hash;
        }

        public static GameObject CreateObject(string name, GameObject parent, params Type[] components) {
            GameObject obj = new GameObject(name, components);
            obj.transform.SetParent(parent.transform, false);
            return obj;
        }

        public static Transform CreateObject(string name, Transform parent, params Type[] components) {
            GameObject obj = new GameObject(name, components);
            obj.transform.SetParent(parent, false);
            return obj.transform;
        }
    }
}
